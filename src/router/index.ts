import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import TimeConvertResults from '../components/TimeConvertResults.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: "/share/:timeZone/:epoch",
      name: 'share',
      component: TimeConvertResults,
      props: true
    }
  ]
})

export default router
